package com.xyz.android_class_9;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by user on 31.01.2018.
 */


/*
https://stackoverflow.com/questions/33162152/storage-permission-error-in-marshmallow

 */
    public class Work9 extends AppCompatActivity {

        public static final int REQUEST_CODE = 1010;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //if permission already granted or api < 23 (without runtime permission)
                workWithExternalStorage();
            } else {
                //if permission not granted
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
            }
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            //if we get permission
            if (REQUEST_CODE == requestCode && grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                Log.v("log","Permission: "+permissions[0]+ "was "+grantResults[0]);
                workWithExternalStorage();
            }
        }


        public void workWithExternalStorage() {
            //here we with permission for external storage access
        }

    }

}
