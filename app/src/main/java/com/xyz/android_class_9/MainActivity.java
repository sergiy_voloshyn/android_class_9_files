package com.xyz.android_class_9;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

/*
https://stackoverflow.com/questions/33162152/storage-permission-error-in-marshmallow

 */
public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FileOutputStream fos = null;



        try {
            fos = openFileOutput("test", Context.MODE_PRIVATE);
            fos.write("some text try".getBytes());

        } catch (Exception ex) {
            ex.printStackTrace();
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Alert")
                    .setMessage(ex.toString())
                    .setCancelable(true)
                    .show();
        } finally {
            try {
                if (fos != null) fos.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        FileInputStream fis = null;
        String line;
        StringBuilder text=new StringBuilder();;
        try {
            fis = openFileInput("test");
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));


            while ((line = reader.readLine()) != null) {
                text.append(line);
            }

        } catch (Exception ex) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Alert")
                    .setMessage(ex.toString())
                    .setCancelable(true)
                    .show();
        } finally {
            if (fis != null)
                try {
                    fis.close();
                } catch (Exception ex) {

                    ex.printStackTrace();
                }

        }
        TextView textView = (TextView) findViewById(R.id.textdata);
        textView.setText(text);


    }
}